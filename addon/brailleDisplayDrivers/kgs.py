# coding: UTF-8
#brailleDisplayDrivers/kgs.py
#A part of NonVisual Desktop Access (NVDA)
#This file is covered by the GNU General Public License.
#See the file COPYING for more details.
#Copyright (C) 2011-2012 Takuya Nishimoto, Masataka Shinke
#Copyright (C) 2013 Masamitsu Misono

import braille
import brailleInput
import inputCore
import hwPortUtils
import time
import tones
import os
from collections import OrderedDict
import ctypes
from ctypes import *
from ctypes.wintypes import *
import config
from logHandler import log
import sys
from kgsbn46 import kgs_dir, fConnection, numCells, myKeyInfoProc
from kgsbn46 import fixConnection, autoConnection, bmConnect, bmDisConnect
from kgsbn46 import InputGesture as BaseInputGesture
from kgsbn46 import BrailleDisplayDriver as BaseDriver

# BOOL (CALLBACK *pHandleKeyInfo)(BYTE info[4])
@WINFUNCTYPE(c_int, POINTER(c_ubyte))
def nvdaKgsHandleKeyInfoProc(lpKeys):
	keys = (lpKeys[0], lpKeys[1], lpKeys[2])
	log.io("keyInfo %d %d %d" % keys)
	log.io("keyInfo hex %x %x %x" % keys)
	names = set()
	routingIndex = None
	if keys[2] &   1: names.add('func1')
	if keys[2] &   2: names.add('func4')
	if keys[2] &   4: names.add('ctrl')
	if keys[2] &   8: names.add('alt')
	if keys[2] &  16: names.add('select')
	if keys[2] &  32: names.add('read')
	if keys[2] &  64: names.add('func2')
	if keys[2] & 128: names.add('func3')
	if keys[0] == 1:
		if keys[1] &   1: names.add('space')
		if keys[1] &   2: names.add('dot6')
		if keys[1] &   4: names.add('dot5')
		if keys[1] &   8: names.add('dot4')
		if keys[1] &  16: names.add('enter')
		if keys[1] &  32: names.add('dot3')
		if keys[1] &  64: names.add('dot2')
		if keys[1] & 128: names.add('dot1')
	elif keys[0] == 2:
		if keys[1] &   1: names.add('esc')
		if keys[1] &   2: names.add('inf')
		if keys[1] &   4: names.add('bs')
		if keys[1] &   8: names.add('del')
		if keys[1] &  16: names.add('ins')
		if keys[1] &  32: names.add('chng')
		if keys[1] &  64: names.add('ok')
		if keys[1] & 128: names.add('set')
	elif keys[0] == 3:
		if keys[1] & 1: names.add('upArrow')
		if keys[1] & 2: names.add('downArrow')
		if keys[1] & 4: names.add('leftArrow')
		if keys[1] & 8: names.add('rightArrow')
	elif keys[0] == 4:
		names.add('route')
		routingIndex = keys[1] - 1
	elif keys[0] == 6:
		if keys[1] & 1: names.add('bw')
		if keys[1] & 2: names.add('fw')
		if keys[1] & 4: names.add('ls')
		if keys[1] & 8: names.add('rs')
	if routingIndex is not None:
		log.io("names %s %d" % ('+'.join(names), routingIndex))
	else:
		log.io("names %s" % '+'.join(names))
	if len(names):
		inputCore.manager.executeGesture(InputGesture(names, routingIndex))
		return True
	return False

myKeyInfoProc = nvdaKgsHandleKeyInfoProc

def getKbdcName(hBrl):
	DEVNAME_JA = u"BMシリーズ機器".encode('shift-jis')
	DEVNAME_EN = u"BM series"
	REGKEY_KBDC110_PATH_JA = r"SOFTWARE\KGS\KBDC110"
	REGKEY_KBDC110_PATH_EN = r"SOFTWARE\KGS\KBDC110-E"
	ret = hBrl.IsKbdcInstalled(REGKEY_KBDC110_PATH_JA)
	if ret:
		devName = DEVNAME_JA
	else:
		ret = hBrl.IsKbdcInstalled(REGKEY_KBDC110_PATH_EN)
		if ret:
			devName = DEVNAME_EN
	return devName

class BrailleDisplayDriver(BaseDriver):
	name = "kgs"
	description = _(u"KGS BrailleMemo series")

	gestureMap = inputCore.GlobalGestureMap({
		"globalCommands.GlobalCommands": {
			"showGui": ("br(kgs):ins",),
			"kb:escape": ("br(kgs):esc",),
			"kb:windows": ("br(kgs):read",),
			"kb:shift": ("br(kgs):select",),
			"kb:control": ("br(kgs):ctrl",),
			"kb:alt": ("br(kgs):alt",),
			"kb:alt+tab": ("br(kgs):alt+inf",),
			"kb:enter": ("br(kgs):enter","br(kgs):ok","br(kgs):set",),
			"kb:space": ("br(kgs):space",),
			"kb:delete": ("br(kgs):del",),
			"kb:backspace": ("br(kgs):bs",),
			"kb:tab": ("br(kgs):inf",),
			"kb:shift+tab": ("br(kgs):select+inf",),
			"kb:upArrow": ("br(kgs):upArrow",),
			"kb:downArrow": ("br(kgs):downArrow",),
			"kb:leftArrow": ("br(kgs):leftArrow",),
			"kb:rightArrow": ("br(kgs):rightArrow",),
			"kb:shift+upArrow": ("br(kgs):select+upArrow",),
			"kb:shift+downArrow": ("br(kgs):select+downArrow",),
			"kb:shift+leftArrow": ("br(kgs):select+leftArrow",),
			"kb:shift+rightArrow": ("br(kgs):select+rightArrow",),
			"review_previousLine": ("br(kgs):bw",),
			"review_nextLine": ("br(kgs):fw",),
			"review_previousWord": ("br(kgs):ls",),
			"review_nextWord": ("br(kgs):rs",),
			"braille_routeTo": ("br(kgs):route",),
			"braille_scrollBack": ("br(kgs):func1","br(kgs):func3+leftArrow",),
			"braille_scrollForward": ("br(kgs):func4","br(kgs):func3+rightArrow",),
			"braille_previousLine": ("br(kgs):func3+upArrow",),
			"braille_nextLine": ("br(kgs):func3+downArrow",),
			"kb:a": ("br(kgs):dot1",),
			"kb:b": ("br(kgs):dot1+dot2",),
			"kb:c": ("br(kgs):dot1+dot4",),
			"kb:d": ("br(kgs):dot1+dot4+dot5",),
			"kb:e": ("br(kgs):dot1+dot5",),
			"kb:f": ("br(kgs):dot1+dot2+dot4",),
			"kb:g": ("br(kgs):dot1+dot2+dot4+dot5",),
			"kb:h": ("br(kgs):dot1+dot2+dot5",),
			"kb:i": ("br(kgs):dot2+dot4",),
			"kb:j": ("br(kgs):dot2+dot4+dot5",),
			"kb:k": ("br(kgs):dot1+dot3",),
			"kb:l": ("br(kgs):dot1+dot2+dot3",),
			"kb:m": ("br(kgs):dot1+dot3+dot4",),
			"kb:n": ("br(kgs):dot1+dot3+dot4+dot5",),
			"kb:o": ("br(kgs):dot1+dot3+dot5",),
			"kb:p": ("br(kgs):dot1+dot2+dot3+dot4",),
			"kb:q": ("br(kgs):dot1+dot2+dot3+dot4+dot5",),
			"kb:r": ("br(kgs):dot1+dot2+dot3+dot5",),
			"kb:s": ("br(kgs):dot2+dot3+dot4",),
			"kb:t": ("br(kgs):dot2+dot3+dot4+dot5",),
			"kb:u": ("br(kgs):dot1+dot3+dot6",),
			"kb:v": ("br(kgs):dot1+dot2+dot3+dot6",),
			"kb:w": ("br(kgs):dot2+dot4+dot5+dot6",),
			"kb:x": ("br(kgs):dot1+dot3+dot4+dot6",),
			"kb:y": ("br(kgs):dot1+dot3+dot4+dot5+dot6",),
			"kb:z": ("br(kgs):dot1+dot3+dot5+dot6",),
			"kb:control+a": ("br(kgs):ctrl+dot1",),
			"kb:control+b": ("br(kgs):ctrl+dot1+dot2",),
			"kb:control+c": ("br(kgs):ctrl+dot1+dot4",),
			"kb:control+d": ("br(kgs):ctrl+dot1+dot4+dot5",),
			"kb:control+e": ("br(kgs):ctrl+dot1+dot5",),
			"kb:control+f": ("br(kgs):ctrl+dot1+dot2+dot4",),
			"kb:control+g": ("br(kgs):ctrl+dot1+dot2+dot4+dot5",),
			"kb:control+h": ("br(kgs):ctrl+dot1+dot2+dot5",),
			"kb:control+i": ("br(kgs):ctrl+dot2+dot4",),
			"kb:control+j": ("br(kgs):ctrl+dot2+dot4+dot5",),
			"kb:control+k": ("br(kgs):ctrl+dot1+dot3",),
			"kb:control+l": ("br(kgs):ctrl+dot1+dot2+dot3",),
			"kb:control+m": ("br(kgs):ctrl+dot1+dot3+dot4",),
			"kb:control+n": ("br(kgs):ctrl+dot1+dot3+dot4+dot5",),
			"kb:control+o": ("br(kgs):ctrl+dot1+dot3+dot5",),
			"kb:control+p": ("br(kgs):ctrl+dot1+dot2+dot3+dot4",),
			"kb:control+q": ("br(kgs):ctrl+dot1+dot2+dot3+dot4+dot5",),
			"kb:control+r": ("br(kgs):ctrl+dot1+dot2+dot3+dot5",),
			"kb:control+s": ("br(kgs):ctrl+dot2+dot3+dot4",),
			"kb:control+t": ("br(kgs):ctrl+dot2+dot3+dot4+dot5",),
			"kb:control+u": ("br(kgs):ctrl+dot1+dot3+dot6",),
			"kb:control+v": ("br(kgs):ctrl+dot1+dot2+dot3+dot6",),
			"kb:control+w": ("br(kgs):ctrl+dot2+dot4+dot5+dot6",),
			"kb:control+x": ("br(kgs):ctrl+dot1+dot3+dot4+dot6",),
			"kb:control+y": ("br(kgs):ctrl+dot1+dot3+dot4+dot5+dot6",),
			"kb:control+z": ("br(kgs):ctrl+dot1+dot3+dot5+dot6",),
			"kb:alt+a": ("br(kgs):alt+dot1",),
			"kb:alt+b": ("br(kgs):alt+dot1+dot2",),
			"kb:alt+c": ("br(kgs):alt+dot1+dot4",),
			"kb:alt+d": ("br(kgs):alt+dot1+dot4+dot5",),
			"kb:alt+e": ("br(kgs):alt+dot1+dot5",),
			"kb:alt+f": ("br(kgs):alt+dot1+dot2+dot4",),
			"kb:alt+g": ("br(kgs):alt+dot1+dot2+dot4+dot5",),
			"kb:alt+h": ("br(kgs):alt+dot1+dot2+dot5",),
			"kb:alt+i": ("br(kgs):alt+dot2+dot4",),
			"kb:alt+j": ("br(kgs):alt+dot2+dot4+dot5",),
			"kb:alt+k": ("br(kgs):alt+dot1+dot3",),
			"kb:alt+l": ("br(kgs):alt+dot1+dot2+dot3",),
			"kb:alt+m": ("br(kgs):alt+dot1+dot3+dot4",),
			"kb:alt+n": ("br(kgs):alt+dot1+dot3+dot4+dot5",),
			"kb:alt+o": ("br(kgs):alt+dot1+dot3+dot5",),
			"kb:alt+p": ("br(kgs):alt+dot1+dot2+dot3+dot4",),
			"kb:alt+q": ("br(kgs):alt+dot1+dot2+dot3+dot4+dot5",),
			"kb:alt+r": ("br(kgs):alt+dot1+dot2+dot3+dot5",),
			"kb:alt+s": ("br(kgs):alt+dot2+dot3+dot4",),
			"kb:alt+t": ("br(kgs):alt+dot2+dot3+dot4+dot5",),
			"kb:alt+u": ("br(kgs):alt+dot1+dot3+dot6",),
			"kb:alt+v": ("br(kgs):alt+dot1+dot2+dot3+dot6",),
			"kb:alt+w": ("br(kgs):alt+dot2+dot4+dot5+dot6",),
			"kb:alt+x": ("br(kgs):alt+dot1+dot3+dot4+dot6",),
			"kb:alt+y": ("br(kgs):alt+dot1+dot3+dot4+dot5+dot6",),
			"kb:alt+z": ("br(kgs):alt+dot1+dot3+dot5+dot6",),
			"kb:.": ("br(kgs):dot2+dot5+dot6",),
			"kb::": ("br(kgs):dot2+dot5",),
			"kb:;": ("br(kgs):dot2+dot3",),
			"kb:,": ("br(kgs):dot2",),
			"kb:-": ("br(kgs):dot3+dot6",),
			"kb:?": ("br(kgs):dot2+dot3+dot6",),
			"kb:!": ("br(kgs):dot2+dot3+dot5",),
			"kb:'": ("br(kgs):dot3",),
		}
	})

class InputGesture(BaseInputGesture):

	source = BrailleDisplayDriver.name
